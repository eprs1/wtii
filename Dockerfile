FROM node:14.8.0-buster
WORKDIR /home/node/wtii/
COPY "package*" ./
RUN npm i
COPY . .
RUN npm run build && chown node:node -R .
ENV NODE_ENV=production
EXPOSE 5000
CMD ["npx","serve","-s","build"]
